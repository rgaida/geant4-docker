# FROM debian:stable-slim AS base
FROM bitnami/minideb:bookworm AS base

LABEL maintainer.name="Roland Gaida"
LABEL maintainer.email="rgaida@mpe.mpg.de"

USER root
WORKDIR /root

COPY packages.txt packages

# - Update base image & install packages --------------------------------------

ENV DEBIAN_FRONTEND=noninteractive

RUN echo 'Acquire::http::Timeout "1200";' > /etc/apt/apt.conf.d/99timeout && \
    apt -qq update && apt upgrade -y && \
    apt -qq install -y --no-install-recommends -o=Dpkg::Use-Pty=0 $(cat packages) > /dev/null && \
    apt autoremove -y && \
    apt clean && \
    rm -rf /var/lib/apt/lists/*

# - Set the locale + timezone -------------------------------------------------

RUN echo "Europe/Berlin" > /etc/timezone && \
    dpkg-reconfigure -f noninteractive tzdata && \
    sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen && \
    sed -i -e 's/# de_DE.UTF-8 UTF-8/de_DE.UTF-8 UTF-8/' /etc/locale.gen && \
    echo 'LANG="en_US.UTF-8"'>/etc/default/locale && \
    locale-gen en_US.UTF-8 && \
    localedef -i en_US -f UTF-8 en_US.UTF-8 && \
    dpkg-reconfigure --frontend=noninteractive locales && \
    update-locale LANG=en_US.UTF-8

ENV LANG en_US.UTF-8
ENV LANGUAGE en_US.UTF-8
ENV LC_ALL en_US.UTF-8

# - Install Geant4 and ROOT ---------------------------------------------------

FROM base AS geant4-install

ARG GEANT4_VERSION="v11.1.2"
ARG ROOT_VERSION="v6.28"

COPY ./binaries/geant4-$GEANT4_VERSION-debian-bin.tar.gz /tmp/
COPY ./binaries/root-$ROOT_VERSION-debian-bin.tar.gz /tmp/
COPY ./binaries/gears-latest-debian-bin.tar.gz /tmp/

RUN tar xzvf /tmp/geant4-$GEANT4_VERSION-debian-bin.tar.gz -C /opt/ > /dev/null && \
    tar xzvf /tmp/root-$ROOT_VERSION-debian-bin.tar.gz -C /opt/ > /dev/null && \
    tar xzvf /tmp/gears-latest-debian-bin.tar.gz -C /opt/ > /dev/null

# - Recompile Gears -----------------------------------------------------------

RUN . /opt/geant4-$GEANT4_VERSION/bin/geant4.sh && \
    cd /opt/gears && \
    cmake . && \
    make 

RUN python3 -m venv /opt/pyenv && \
    . /opt/pyenv/bin/activate && \
    pip install geant4-pybind

# - Cleanup
    
RUN rm -rf /tmp/geant4-$GEANT4_VERSION-debian-bin.tar.gz && \
    rm -rf /tmp/root-$ROOT_VERSION-debian-bin.tar.gz && \
    rm -rf /tmp/gears-latest-debian-bin.tar.gz && \
    rm -rf /root/packages

# - Setup environment ---------------------------------------------------------

FROM base AS final-stage

ARG GEANT4_VERSION="v11.1.2"
ARG ROOT_VERSION="v6.28"

COPY --from=geant4-install /opt /opt

ADD .bash_aliases /root/

RUN echo "if [ -f ~/.bash_aliases ]; then . ~/.bash_aliases; fi" >> ~/.bashrc && \
    echo "source /opt/geant4-$GEANT4_VERSION/bin/geant4.sh" >> ~/.bashrc && \
    echo "source /opt/gears/gears.sh" >> ~/.bashrc && \
    echo "source /opt/root/bin/thisroot.sh" >> ~/.bashrc && \
    echo "source /opt/pyenv/bin/activate" >> ~/.bashrc && \
    mkdir -p -v /var/geant4/workspace

# ENV G4FORCENUMBEROFTHREADS 4
ENV G4EXAMPLES "/opt/geant4-$GEANT4_VERSION/share/Geant4/examples"

WORKDIR /var/geant4/workspace

CMD ["/bin/bash"]
