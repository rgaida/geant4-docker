@echo off
docker build --build-arg TAG="v11.1.3" --build-arg MULTITHREADING="ON" --build-arg INCLUDE_GEARS="Y" --output=./binaries --target=artifacts -f Dockerfile_geant4 .