alias grep='grep --color=auto'
alias ls='ls --color=auto'
alias ll='ls -lh --group-directories-first -X'