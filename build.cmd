@echo off
docker build --pull --rm --build-arg GEANT4_VERSION="v11.1.3-mt" --build-arg ROOT_VERSION="v6.28.06" -f Dockerfile -t gitlab-registry.mpcdf.mpg.de/rgaida/geant4-docker:11.1.3 .
