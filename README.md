# geant4-docker

Linux Docker Environment with Geant4 and Root Support

Base image: Bitnami Minideb (Debian Bookworm)

Included software:

* Geant4 v11.1.3 with Pybind and GEARS
* Root v6.28.6 (includes PyRoot library)
* Python v3.11.2, Pip v23.0.1

Geant4 was build with the following flags enabled:

```
GEANT4_INSTALL_DATA
GEANT4_USE_GDML
GEANT4_USE_OPENGL_X11
GEANT4_USE_QT
GEANT4_USE_RAYTRACER_X11
GEANT4_USE_SYSTEM_ZLIB
```
Multithreading is enabled by default.

## What is Geant4?

[Geant4](https://geant4.web.cern.ch/) (Geometry And Tracking) is a software toolkit developed by CERN for simulating the passage of particles through matter. It's implemented in C++, utilizing an object-oriented design for flexibility and extensibility.

The toolkit allows researchers to model and track the interactions between particles and materials, simulating processes such as energy loss, deflection, and the creation of secondary particles. Its capabilities include describing complex geometric configurations, representing results realistically, and visualizing detector geometries and particle trajectories.

Geant4 is extensively used across various fields including high-energy particle physics, nuclear physics, space science, and medical physics. It's instrumental in research into fundamental properties of matter, designing detectors for high-energy physics experiments, planning radiation therapy treatments in medical physics, and predicting performance of spacecraft instruments in harsh cosmic environments.

## Getting started

Make sure docker is installed and running on your platform. To make use of Geant4's visualization capabilities, you'll need to have an X server installed (Linux/Windows) or WSLg (Windows).

### 1 - Your fastest way to get started: Use a prebuilt image

If you prefer not to build a container image yourself, you can utilize the prebuilt image available in this repository. The image is about 5 GB in size.

To do so, proceed directly to step 3 and replace `-t geant4-docker` with `-t gitlab-registry.mpcdf.mpg.de/rgaida/geant4-docker`.

You can update the image at any time by pulling the latest version from the registry:

```bash
docker pull gitlab-registry.mpcdf.mpg.de/rgaida/geant4-docker
```

If you are using a persistent container (see step 3b), remember to recreate your container if you pull a newer version of the image. This means that any changes you made to the previous container will be lost, except for the contents in the mounted volume.

### 2 - Build the container image

Download and extract or clone the repository. Navigate to the directory containing the `Dockerfile`.

Before building the actual container image (step 2c), you need to compile Geant4 and Root binaries. This is done separately in their own docker environments. The binaries are then exported to the `binaries` directory.

#### 2a - Compiling Geant4

Execute the following command to download and compile Geant4 sources in a docker container and export the result to the `binaries` directory:

```bash
docker build --build-arg TAG="v11.1.2" --build-arg MULTITHREADING="ON" --build-arg INCLUDE_GEARS="Y" --output=./binaries --target=artifacts -f Dockerfile_geant4 .
```

This command will build Geant4 v11.1.2 with multithreading enabled and additionally compiles the latest GEARS version (default). It exports the archived binaries to the `binaries` directory as `geant4-v11.1.2-mt-debian-bin.tar.gz` and `gears-latest-debian-bin.tar.gz` archives.

You can change the TAG argument to build a different version of Geant4. The available tags can be found in the [Geant4 repository](https://gitlab.cern.ch/geant4/geant4/-/tags). The packages_geant4.txt file includes all packages that are required to build Geant4. If you want to add or remove packages, you need to edit this file.

To build a single-threaded version of Geant4, set the MULTITHREADING argument to "OFF". To avoid building GEARS, set the INCLUDE_GEARS argument to "N".

Repeat this step for each Geant4 version you want to use and skip to step 2c if you already have Root binaries.

#### 2b - Compiling Root

Execute the following command to download and compile Root sources in a docker container and export the result to the `binaries` directory:

```bash
docker build --build-arg TAG="v6-28-06" --output=./binaries --target=artifacts -f Dockerfile_root .
```

This command will build ROOT v6.28.06 and export the archived binaries to the `binaries` directory as `root-v6.28.06-debian-bin.tar.gz` archive.

Again, you can change the TAG argument to build a different version of ROOT. The available tags can be found in the [ROOT repository](https://github.com/root-project/root/tags). The packages_root.txt file includes all packages that are required to build ROOT. If you want to add or remove packages, you need to edit this file.

Repeat this step for each ROOT version you want to use.

#### 2c - Building the container image

Before proceeding with this step, make sure you have already compiled Geant4 and Root binaries available as .tar.gz archives in the binaries directory (from steps 2a and 2b). The version number in the archive names must match the version numbers specified in the `--build-arg` arguments.

Execute the command below to actually build the docker image. Please note, this step only needs to be completed once, provided the contents of the `Dockerfile` remain unaltered and as long as no new Geant4 or ROOT binaries are required.

```bash
docker build --pull --rm --build-arg GEANT4_VERSION="v11.1.2" --build-arg ROOT_VERSION="v6.28.06" -f Dockerfile -t geant4-docker .
```

### 3 - Run the container image

#### 3a - Run a temporary container using the image

A temporary container will be deleted once it is stopped or the last interatve shell is closed. Any changes made to the container will be lost with the exception of the contents in the mounted volume. Each run will start with a fresh container.

Option 1 (X server)

If you're running Linux or Windows with an X server installed, launch the container using the following command (replace `<mylocalworkspace>` with a valid local directory - this is where your data will persist):

```bash
docker run -it --rm --name geant4 -h container -e DISPLAY=host.docker.internal:0.0 -v <mylocalworkspace>:/var/geant4/workspace -v /tmp/.X11-unix:/tmp/.X11-unix -t geant4-docker
```

> This command runs a Docker container named `geant4` based on the image `geant4-docker`, and mounts the `<mylocalworkspace>` directory as a volume in the container. It sets the hostname of the container to "container", and sets the `DISPLAY` environment variable to `host.docker.internal:0.0` to allow the container to connect to the host machine's X server. The `-it` and `--rm` options are used to run the container in interactive mode, remove the container when it is stopped, and allocate a pseudo-TTY respectively. The `-v` option is used to mount the `/tmp/.X11-unix` directory on the host machine to the same directory in the container, which allows the container to access the host machine's X server.

Option 2 (WSLg)

If you're running Windows with WSL and WSLg installed, launch the container using the following command (replace `<mylocalworkspace>` with a valid local directory - this is where your data will persist):

```bash
docker run -it --rm --name geant4 -h container -e DISPLAY=:0 -v <mylocalworkspace>:/var/geant4/workspace -v /run/desktop/mnt/host/wslg/.X11-unix:/tmp/.X11-unix -v /run/desktop/mnt/host/wslg:/mnt/wslg -t geant4-docker
```

> This command is similar to the one above, but sets the `DISPLAY` environment variable to `:0` to allow the container to connect to the host machine's X server. The `-v` option is used to mount the `/run/desktop/mnt/host/wslg` directory on the host machine to the same directory in the container, which allows the container to access the host machine's X server.

**Important:** Any changes outside `/var/geant4/workspace` will be lost once the container is stopped.

#### 3b - Run a persistent container using the image

A persistent container will not be deleted once it is stopped or the last interatve shell is closed. Any changes made to the container will be persisted, even those changes outside of mounted volumes. Each run will start with the same container.

The `docker run` command has only to be executed once(!) after each image update:

Option 1 (X server)

```bash
docker run -it --name geant4 -h container -e DISPLAY=host.docker.internal:0.0 -v <mylocalworkspace>:/var/geant4/workspace -v /tmp/.X11-unix:/tmp/.X11-unix -t geant4-docker
```

Option 2 (WSLg)

```bash
docker run -it --name geant4 -h container -e DISPLAY=:0 -v <mylocalworkspace>:/var/geant4/workspace -v /run/desktop/mnt/host/wslg/.X11-unix:/tmp/.X11-unix -v /run/desktop/mnt/host/wslg:/mnt/wslg -t geant4-docker
```

To start the container again, use `docker start -i geant4`.

To start another shell in the container while it is already running, use `docker exec -it geant4 /bin/bash`.

Please note that if you rebuild the container image or pull a newer version of the container image, you will need to recreate the container using the  `docker run` command. This means that any changes you made to the previous container will be lost, except for the contents in the mounted volume.

### 5 - Compile and execute an example

All Geant4 examples can be found in G4EXAMPLES (`/opt/geant4-v11.1.2/share/Geant4/examples`). GEARS tutorial files can be found in `/opt/gears/tutorials`.

Let's compile and execute one of the basic examples, B1:

Run the image and start in your working directory `/var/geant4/workspace`.

Compile the source code:

```bash
mkdir buildB1 && cd ./buildB1
cmake $G4EXAMPLES/basic/B1
make
```

(you can use the -j parameter to speed up compilation, e.g. `make -j 4`; the number should be equal to the number of CPU cores available to the container)

Execute the compiled binary:

```bash
./exampleB1
```

![Screenshot](docs/images/screenshot_exampleB1.png)

Your host platform has to support X11 applications (e.g. a running X11 server or WSLg).

## Root: TBrowser vs RBrowser

RBrowser is a ROOT-aware web browser based on Chromium Embedded Framework (CEF). It is a modern, lightweight and fast web browser that can be used to browse ROOT files and objects. It is the default browser in ROOT 6.26 and later.

From the [Root documentation](https://root.cern/doc/master/rbrowser.html):

> Starting from ROOT version 6.26/00, RBrowser (...) is automatically invoked with new TBrowser.
> 
> If for any reasons RBrowser does not provide required functionality, one always can disable it. Either by specifying `root --web=off` when starting ROOT or by setting `Browser.Name: TRootBrowser` in `rootrc` file.

## Python bindings

### Pybind

The image contains geant4_pybind from https://github.com/HaarigerHarald/geant4_pybind.

Minimal example (just starts a Geant4 shell):

```Python
from geant4_pybind import *
import sys

ui = G4UIExecutive(len(sys.argv), sys.argv)
ui.SessionStart()
```

Geant4 basic examples ported to Python can be found here: https://github.com/HaarigerHarald/geant4_pybind/tree/main/examples

Example B5 with `/vis/viewer/set/viewpointThetaPhi 70 20`, `/vis/viewer/set/style surface` and `/run/beamOn 1`:

![Screenshot](docs/images/screenshot_exampleB5_python.png)

### pyROOT

Minimal example for using pyROOT:

```Python
import ROOT
f = ROOT.TF1("f1", "sin(x)/x", 0., 10.)
f.Draw()
```

![Screenshot](docs/images/screenshot_example_pyroot.png)

## Installing additional Python packages

If you want to persist the installation of additional python packages, you need to create a docker volume and mount it to `/opt/pyenv`.

1. Create a docker volume:

```bash
docker volume create geant4-pyenv
```

2. Run the container with the volume mounted (X11 server):

You need to mount the created volume to `/opt/pyenv`. Add `-v geant4-pyenv:/opt/pyenv` to the docker run command:

```bash
docker run -it --rm -h container -e DISPLAY=host.docker.internal:0.0 -v <mylocalworkspace>:/var/geant4/workspace -v /tmp/.X11-unix:/tmp/.X11-unix -v geant4-pyenv:/opt/pyenv -t geant4-docker
```

or (WSLg)

```bash
docker run -it --rm -h container -e DISPLAY=:0 -v <mylocalworkspace>:/var/geant4/workspace -v /run/desktop/mnt/host/wslg/.X11-unix:/tmp/.X11-unix -v /run/desktop/mnt/host/wslg:/mnt/wslg -v geant4-pyenv:/opt/pyenv -t geant4-docker
```

respectively.